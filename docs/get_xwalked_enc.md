# Encounters

TODO:

## Master -> Xwalked
Required columns in `enc` data.table:
- `ptid`: can be another name
- `start_date`: exact name
- `end_date`: exact name (optional)

Main script [get_xwalked_enc.R](../R/get_xwalked_enc.R):
1. Check that ptid type in `enc` data.table is the same as ptid type in `cohort`
data.table.
1. Merge `enc` data.table with `cohort` data.table by `ptid`.
    - This is to get the chunk_id col.
1. If end_date is missing
    - Add column: `end_date` == 'start_date'
    - Add columns: `length_of_stay_hours` = 0, `length_of_stay_days` = 0
1. Add `max_lookback` column = 'start_date' + 'lookback'
    - Used in foverlaps

### Helper functions
- [convert_ptid_type.R](../R/convert_ptid_type.R)
