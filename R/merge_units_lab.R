#' merge_units_lab
#' @description merge lab_units xwalk by lab_cat,
#'
#' @param DT data.table of lab with columns lab_cat, lab_units
#'
#' @return updated DT
#' @export

merge_units_lab <- function(DT) {
    print("...merge_units_lab")
	# merge
	DT_xwalked <- merge(DT, lab_units_xwalk_clean, by=c("lab_cat","lab_units"), all.x=TRUE)
	DT_xwalked[!is.na(i.lab_units),lab_units:=i.lab_units]
	DT_xwalked[,i.lab_units:=NULL]

	# lab_cat:lab_units combo must appear at least 100 times (to drop less common unit variations)
	DT_xwalked[,N:=.N,by=c("lab_cat","lab_units")]
	DT_xwalked %<>% .[N>100,]
	DT_xwalked[,N:=NULL]

	return(DT_xwalked)
}
