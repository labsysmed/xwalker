#' drop_outliers
#' @description drop values outside 99.9% and 0.1% quantiles (useful for lvs and lab)
#'
#' @param DT data.table 
#' @param bycol which columns to calculate quantiles by
#' @param value.var name of column containing numeric data to use for quantile calculation
#'
#' @return updated DT
#' @export

drop_outliers <- function(DT, bycol, value.var) {

  DT[,upper:=quantile(get(value.var), probs=0.9999, na.rm=TRUE), by=bycol]
  DT[,lower:=quantile(get(value.var), probs=0.0001, na.rm=TRUE), by=bycol]
  ranges <- DT[,.(upper,lower,get(bycol))] %>% unique
  ranges %>% print(nrow=nrow(.))
  DT %<>% .[get(value.var)<upper & get(value.var)>lower]
  DT[,upper:=NULL][,lower:=NULL]
  return(DT)
}
