% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data.R
\docType{data}
\name{dem_gps_xwalk_clean}
\alias{dem_gps_xwalk_clean}
\title{dem_gps_xwalk_clean}
\format{A data.table with 33144 rows and 5 variables:
\describe{
  \item{zip_code}{}
  \item{latitude}{}
  \item{longitude}{}
  \item{dist_BWH_miles}{}
  \item{dist_BWH_log_miles}{}
}}
\source{
/data/zolab/methods_new/dem/census_data_by_zip_code.txt
}
\usage{
data(dem_gps_xwalk_clean)
}
\description{
xwalk for gps xwalk
}
\keyword{datasets}
