# Diagnoses

TODO:
- investigate source of original data xwalk files
    - look into EDW Reference tables for ICD9-ICD10, ICD10-ICD9, SNOMED
- function should take start_date parameter for name of start_date column
(in `dia` this would be the 'dia_date' column)

## Master -> Xwalked
Required columns in `dia` data.table:
- `ptid`: can be another name
- `dia_code_type`: exact name
- `dia_code`: exact name

Main script [get_xwalked_dia.R](../R/get_xwalked_dia.R):
1. Check that ptid type in `dia` data.table is the same as ptid type in `cohort`
data.table.
1. Merge `dia` data.table with `cohort` data.table by `ptid`.
    - This is to get the chunk_id col.
1. Subset to `dia_code_type` == 'icd9'.
    - Print dia_code_type that are != 'icd9'
1. Merge zocat xwalk by 'dia_code'
    - Adds columns: `long_desc`, `short_desc`, `zc_cat_name_detail`, `zc_cat_name`, `zc_cat_num`
    - Print 'dia_code' that did not match to zocat
1. Merge ccs_dia
    - Merge dia_ccs_single_xwalk_clean xwalk by 'dia_code' ('icd_9_cm_code' in xwalk)
        - Adds columns: `ccs_category`, `ccs_category_description`, `icd_9_cm_code_description`
        - Print 'dia_code' that did not map to single CCS
    - Merge dia_ccs_multi_xwalk_clean xwalk by 'dia_code' ('icd_9_cm_code' in xwalk)
        - Adds columns: `ccs_lvl_1`, `ccs_lvl_1_label`, `ccs_lvl_2`, `ccs_lvl_2_label`, `ccs_lvl_3`, `ccs_lvl_3_label`, `ccs_lvl_4`, `ccs_lvl_4_label`
        - Print 'dia_code' that did not map to multi CCS
1. Merge gagne xwalk by 'dia_code' ('code' in xwalk)
    - Adds columns: `gagne_cat`, `gagne_weight`
1. Add `max_lookback` column = 'dia_date' + 'lookback'
    - Used in foverlaps

### Helper functions
- [convert_ptid_type.R](../R/convert_ptid_type.R)
- [merge_zocat.R](../R/merge_zocat.R)
- [merge_ccs_dia.R](../R/merge_ccs_dia.R)
- [merge_gagne.R](../R/merge_gagne.R)

## Xwalks
In `data/`:
- zocat.rda
- dia_ccs_single_xwalk_clean.rda
- dia_ccs_multi_xwalk_clean.rda
- dia_gagne_xwalk_clean.rda

Distinction among these xwalks:
- Gagne features - there are about 20 of these. They are commonly used in health policy research. So when we need something easy to explain (e.g., a health measure for racial bias paper) or to put in a Table 1 (to summarize what illnesses people have), we use these
- zocats - there are 200+ of these. They are good for features because they are rich and detailed. They are an adaptation of the CCS single-level categorization, with improvements made by Ziad. They include the Gagne cats and also have many more.
- ccs_multi - these aggregate CCS single level categories into higher level categories, like “Cardiovascular” or “infectious”.

To summarize: when we need a small set of “industry standard” illnesses, we use Gagne. When we need features, we use zocats + CCS-multi.

### zocat
source code: [data-raw/zocat.R](../data-raw/zocat.R)
- rows: 15071, cols: 6
    1. dia_code
    1. long_desc
    1. short_desc
    1. zc_cat_name_detail
    1. zc_cat_name
    1. zc_cat_num

### dia_ccs_single_xwalk_clean
source code: [data-raw/clean_dia_ccs_single_xwalk.R](../data-raw/clean_dia_ccs_single_xwalk.R)
- rows: 15073, cols: 6
    1. icd_9_cm_code
    1. ccs_category
    1. ccs_category_description
    1. icd_9_cm_code_description
    1. optional_ccs_category
    1. optional_ccs_category_description

Original source: /data/zolab/methods_new/ccs/dia/single_level/ccs_single_dia_2015.csv

### dia_ccs_multi_xwalk_clean
source code: [data-raw/clean_dia_ccs_multi_xwalk](../data-raw/clean_dia_ccs_multi_xwalk.R)
- rows: 15072, cols: 9
    1. icd_9_cm_code
    1. ccs_lvl_1
    1. ccs_lvl_1_label
    1. ccs_lvl_2
    1. ccs_lvl_2_label
    1. ccs_lvl_3
    1. ccs_lvl_3_label
    1. ccs_lvl_4
    1. ccs_lvl_4_label

Original source: /data/zolab/methods_new/ccs/dia/multi_level/ccs_multi_dia_2015.csv

### dia_gagne_xwalk_clean
source code: [data-raw/clean_dia_gagne_xwalk.R](../data-raw/clean_dia_gagne_xwalk.R)
- rows: 15268, cols: 3
    1. gagne_cat
    1. code
    1. gagne_weight

Original source: /data/zolab/methods_new/dia/gagne_codes/Gagne_codes.csv
