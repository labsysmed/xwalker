# This file was derived from RPDR lab data
# https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/pre_xwalk_helpers/lab_cat_to_loinc_xwalk.R
library(data.table)

# original source: /data/zolab/methods_new/lab/lab_cat_to_loinc_xwalk.csv
lab_xwalk <- fread("lab_cat_to_loinc_xwalk.csv")

lab_cat_to_loinc_xwalk_clean <- lab_xwalk

devtools::use_data(lab_cat_to_loinc_xwalk_clean, overwrite = TRUE)
