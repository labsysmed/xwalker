# xwalkeR R package

### Installation
Please see [the knowledge base](https://gitlab.com/labsysmed/zolab/knowledge_base/blob/master/R/R_packages.md) for how to update and install existing R packages.
This package depends on version 1.0 of the standardizeR package.

### Usage example

### Status
xwalking functions (and associated dependencies) for the following are completed:
- dem
- dia
- enc
- lvs
- lab
- med
- prc

### Maintaining
Please see `/docs` for a deeper understanding of package.
