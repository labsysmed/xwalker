# Medications

TODO:
- look into med_rxnorm_xwalk_clean
    - was specific for BWH ED cohort
    - https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/pre_xwalk_helpers/query_rxnorm.R

## Master -> Xwalked
Required columns in `med` data.table:
- `ptid`: can be another name
- `med_name`: exact name

Main script [get_xwalked_med.R](../R/get_xwalked_med.R):
1. Check that ptid type in `med` data.table is the same as ptid type in `cohort`
data.table.
1. Merge `med` data.table with `cohort` data.table by `ptid`.
    - This is to get the chunk_id col.
1. Merge med_rxnorm_xwalk_clean by 'med_name' ('med_name_raw' in xwalk)
    - Add columns: `atc_code`, `rxcui`, `rxnorm_name`, `atc_level_name`
1. Add `max_lookback` column = 'med_date' + 'lookback'
    - Used in foverlaps

### Helper functions
- [convert_ptid_type.R](../R/convert_ptid_type.R)
- [merge_rxnorm.R](../R/merge_rxnorm.R)

## Xwalks
In `data/`:
- med_rxnorm_xwalk_clean.rda

### med_rxnorm_xwalk_clean
source code: [data-raw/med_rxnorm_xwalk_clean.R](../data-raw/med_rxnorm_xwalk_clean.R)
- rows: 535245, col: 5
    1. atc_code
    1. med_name_raw
    1. rxcui
    1. rxnorm_name
    1. atc_level_name

Original source: /data/zolab/methods_new/med/rxnorm_med_name_xwalk_bwh_ed_2010_2015.csv
- This file was specific for BWH ED cohort
- https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/pre_xwalk_helpers/query_rxnorm.R
