% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/shorten_label.R
\name{shorten_label}
\alias{shorten_label}
\title{shorten_label}
\usage{
shorten_label(descriptions, max_length = 50, beg_fraction = 0.8)
}
\arguments{
\item{descriptions}{vector of descriptions to be abbreviated}

\item{max_length}{max character length of shortened labels (default: 50)}

\item{beg_fraction}{number of characters to to be kept from the label's beginning (as a % of max_length characters)  (default: 0.8)}
}
\value{
descriptions reformatted
}
\description{
shortens a column of labels
}
