# get_xwalked

There are `get_xwalked_*.R` scripts for the following EHR file types:
- [dem](get_xwalked_dem.md): demographics
- [dia](get_xwalked_dia.md): diagnoses
- [enc](get_xwalked_enc.md): encounters
- [lab](get_xwalked_lab.md): laboratory values
- [lvs](get_xwalked_lvs.md): longitudinal vital signs
- [med](get_xwalked_med.md): medications
- [prc](get_xwalked_prc.md): procedures

The template for `get_xwalked_*.R` is as follows:

```
#' get_xwalked_[file]
#' @description xwalk [file]
#'
#' @import data.table
#' @import magrittr
#'
#' @param DT data.table of [file] in standard master format
#' @param cohort cohort file containing ptid & chunk_id
#' @param lookback max lookback period to be used (needed for feature generation)
#' @param ptid column name in cohort file for patient identifier
#' @param chunk_id column name in cohort file for chunk id (ex: chunk1K)
#'
#' @return updated DT
#' @export

get_xwalked_[file] <- function(DT, cohort, lookback, ptid="ptid", chunk_id="chunk_id") {
    print("...get_xwalked_[file]")
    print(nrow(DT))

    ### check that type of ptid in DT is the same as ptid in cohort
    ### otherwise, convert to numeric
    temp <- convert_ptid_type(DT, cohort, ptid)
    DT <- temp$DT
    cohort <- temp$cohort

    ### merge cohort
    print("...merging cohort")
    DT <- merge(DT, cohort[,.(ptid,chunk_id)], by="ptid", all=FALSE)
    print(nrow(DT))		


    [xwalking steps such as merging here]

    # add max_lookback for use in foverlaps
    DT_mod[,max_lookback:=[date_colname]+lookback]

    ### return
    print(nrow(DT))
    return(DT_mod)
}
```

## TODOs
- make sure 'ptid' type in DT is the same as 'ptid' type in cohort (shouldn't need to check in
`get_xwalked_*.R`)
- make 'date_colname' a function parameter (used for 'max_lookback' column)
- move `xwalk()` from [mgh_primary project](https://gitlab.com/labsysmed/zolab-projects/mgh_primary/blob/master/code/02_xwalked/master_to_xwalked.R) to this package
    - move .sqlite save steps to a function
