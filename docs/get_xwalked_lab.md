# Laboratory Values

TODO:
- why was lab_cat_to_loinc_xwalk_clean derived from RPDR lab data?
    - https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/pre_xwalk_helpers/lab_cat_to_loinc_xwalk.R
- why was lab_units_xwalk_clean derived from RPDR lab data?
    - https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/xwalk_functions/lab_xwalk.R#L57

## Master -> Xwalked
Required columns in `lab` data.table:
- `ptid`: can be another name
- `loinc_id`: exact name
- `lab_cat`: exact name
- `lab_units`: exact name

Main script [get_xwalked_lab.R](../R/get_xwalked_lab.R):
1. Check that ptid type in `lab` data.table is the same as ptid type in `cohort`
data.table.
1. Merge `lab` data.table with `cohort` data.table by `ptid`.
    - This is to get the chunk_id col.
1. Merge lab_cat_to_loinc_xwalk_clean by 'loinc_id' ('loinc_code' in xwalk)
    - Adds column: `lab_cat`
1. Merge lab_units by 'lab_cat', 'lab_units'
    - Updates 'lab_units' using 'i.lab_units' in xwalk. But lab_cat, lab_units
    combo must appear at least 100 times (to drop less common unit variations)
1. Add `max_lookback` column = 'lab_date' + 'lookback'
    - Used in foverlaps

### Helper functions
- [convert_ptid_type.R](../R/convert_ptid_type.R)
- [merge_lab_cat.R](../R/merge_lab_cat.R)
- [merge_units_lab.R](../R/merge_units_lab.R)

## Xwalks
In `data/`:
- lab_cat_to_loinc_xwalk_clean.rda
- lab_units_xwalk_clean.rda

### lab_cat_to_loinc_xwalk_clean
source code: [data-raw/lab_cat_to_loinc_xwalk.R](../data-raw/lab_cat_to_loinc_xwalk.R)
- rows: 690, cols: 2
    1. loinc_code
    1. lab_cat

Original source: /data/zolab/methods_new/lab/lab_cat_to_loinc_xwalk.csv
- This file was derived from RPDR lab data
- https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/pre_xwalk_helpers/lab_cat_to_loinc_xwalk.R

### lab_units_xwalk_clean
source code: [data-raw/lab_units_xwalk.R](../data-raw/lab_units_xwalk.R)
- rows: 114, cols: 4
    1. lab_cat
    1. lab_units
    1. N
    1. i.lab_units

Original source: /data/zolab/methods_new/lab/bwh_2010_2015_lab_units_xwalk.csv
- This file was derived from RPDR lab data?
- https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/xwalk_functions/lab_xwalk.R#L57

## TODO
Stephen noticed a few discrepancies with the lab units for ddimer:
1. (NH) lab_qdmid 202921 should be nano, not micro
1. (NSMC) lab_qdmid 254986 should be micro, not nano
1. (SRH) lab_qdmid 254979 should be micro, not nano 
1. (NWH) Beginning 2013-02-19 08:50:00, lab_qdmid 202919 should be nano, not micro

TODO: Add a xwalk function for cleaning thes ddimer units.