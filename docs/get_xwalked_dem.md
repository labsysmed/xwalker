# Demographics

TODO:
- include ACS income data?

## Master -> Xwalked
Required columns in `dem` data.table:
- `ptid`: can be another name
- `birth_date`: exact name
- `race`: exact name
- `sex`: exact name
- `zip_code`: exact name (optional)

Main script [get_xwalked_dem.R](../R/get_xwalked_dem.R):
1. Check that ptid type in `dem` data.table is the same as ptid type in `cohort`
data.table.
1. Merge `dem` data.table with `cohort` data.table by `ptid`.
    - This is to get the t0 and chunk_id col.
1. Calculate age: t0 - birth_date
    - Adds column: `age`
1. Add race indicators using the `race` col.
    - Adds columns: `race_black`, `race_hispanic`, `race_white`, `race_other`
    which are 0/1 indicators
1. Add sex indicators using the `sex` col.
    - Adds column: `sex_female` which is 0/1 indicator
1. If `zip_code` is a column in `dem` data.table:
    - Merge in 'dem_income_cat_xwalk_clean' xwalk by 'zip_code'
        - Adds columns: `latitude`, `longitude`, `dist_BWH_miles`, `dist_BWH_log_miles`
    - Merge in 'dem_gps_xwalk_clean' xwalk by 'zip_code'
        - Adds columns: `agi_under_25K`, `agi_25K_to_50K`, `agi_50K_to_75K`, `agi_75K_to_100K`, `agi_100K_to_200K`, `agi_above_200K`

### Helper functions
- [convert_ptid_type.R](../R/convert_ptid_type.R)
- [add_race_indicators.R](../R/add_race_indicators.R)
- [add_sex_indicators.R](../R/add_sex_indicators.R)
- [merge_zip_code.R](../R/merge_zip_code.R)

## Xwalks
In `data/`:
- dem_gps_xwalk_clean.rda
- dem_income_cat_xwalk_clean.rda

### dem_gps_xwalk_clean
source code: [data-raw/clean_dem_gps_xwalk.R](../data-raw/clean_dem_gps_xwalk.R)
- rows: 33144, cols: 5
    1. zip_code
    1. latitude
    1. longitude
    1. dist_BWH_miles
    1. dist_BWH_log_miles

Original source: /data/zolab/methods_new/dem/census_data_by_zip_code.txt
- downloaded from https://www.census.gov/geo/maps-data/data/gazetteer2017.html

### dem_income_cat_xwalk_clean
source code: [data-raw/clean_dem_income_xwalk.R](../data-raw/clean_dem_income_xwalk.R)
- rows: 27681, cols: 7
    1. zip_code
    1. agi_under_25K
    1. agi_25K_to_50K
    1. agi_50K_to_75K
    1. agi_75K_to_100K
    1. agi_100K_to_200K
    1. agi_above_200K

Original source: /data/zolab/methods_new/dem/irs_data_by_zip_code.csv
- downloaded from https://www.irs.gov/statistics/soi-tax-stats-individual-income-tax-statistics-zip-code-data-soi

## Raw Xwalks
`data-raw/dem_boston_zipcode.csv`
- list of Boston zip codes
- source: https://www.boston.gov/sites/default/files/zipcodes_tcm3-47884.pdf

`data-raw/dem_greater_boston_zipcode.csv`
- list of Greater Boston zip codes found by clicking on the map below
- source: https://www.unitedstateszipcodes.org/zip-code-radius-map.php
