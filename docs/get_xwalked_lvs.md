# Longitudinal Vital Signs

TODO:
- check if cleaning needs to be done as before: https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/xwalk_functions/lvs_xwalk.R

## Master -> Xwalked
Required columns in `lvs` data.table:
- `ptid`: can be another name
- `lvs_date`: exact name

Main script [get_xwalked_lvs.R](../R/get_xwalked_lvs.R):
1. Check that ptid type in `lvs` data.table is the same as ptid type in `cohort`
data.table.
1. Merge `lvs` data.table with `cohort` data.table by `ptid`.
    - This is to get the chunk_id col.
1. Add `max_lookback` column = 'lvs_date' + 'lookback'
    - Used in foverlaps

### Helper functions
- [convert_ptid_type.R](../R/convert_ptid_type.R)
