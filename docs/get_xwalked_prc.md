# Procedures

TODO:
- figure out source of prc_ccs_multi_xwalk_clean
- why did we use RPDR file to create prc_ccs_single_xwalk_clean xwalk?

## Master -> Xwalked
Required columns in `prc` data.table:
- `ptid`: can be another name
- `prc_code_type`: exact name
- `prc_code_raw`: exact name
- `prc_date`: exact name

Main script [get_xwalked_prc.R](../R/get_xwalked_prc.R):
1. Check that ptid type in `prc` data.table is the same as ptid type in `cohort`
data.table.
1. Merge `prc` data.table with `cohort` data.table by `ptid`.
    - This is to get the chunk_id col.
1. Print 'prc_code_type' not in ["icd9","cpt","hcpcs"]
    - Currently don't have xwalks for prc_code_type not in this list
1. Standardize 'prc_code_type' == "icd9"
    - Adds col: `prc_code_std` with standardized code
1. Standardize 'prc_code_type' == "cpt", "hcpcs"
    - Adds col: `prc_code_std` with standardized code
1. Drop rows where 'prc_code_std' is NA (prc code could not be standardized).
1. Merge ccs_prc
    - Merge prc_ccs_single_xwalk_clean xwalk by 'prc_code_std', 'prc_code_type' ('code', 'flag' in xwalk)
        - Adds columns: `ccs_cat`, `ccs_category_description`, `icd_9_cm_code_description`
        - Print 'prc_code_std' that did not map to single CCS
    - Merge prc_ccs_multi_xwalk_clean xwalk by 'prc_code_std' ('prc_code' in xwalk)
        - Adds columns: `ccs_multi_cat_num_1`, `ccs_multi_cat_name_1`, `ccs_multi_cat_num_2`, `ccs_multi_cat_name_2`, `ccs_multi_cat_num_3`, `ccs_multi_cat_name_3`
        - Print 'prc_code_std' that did not map to multi CCS
1. Add `max_lookback` column = 'prc_date' + 'lookback'
    - Used in foverlaps

### Helper functions
- [convert_ptid_type.R](../R/convert_ptid_type.R)
- [standardize_icd9_prc_codes.R](../R/standardize_icd9_prc_codes.R)
- [standardize_cpt_prc_codes.R](../R/standardize_cpt_prc_codes.R)
- [merge_ccs_prc.R](../R/merge_ccs_prc.R)

## Xwalks
In `data/`:
- prc_ccs_multi_xwalk_clean.rda
- prc_ccs_single_xwalk_clean.rda

### prc_ccs_multi_xwalk_clean
source code: [data-raw/clean_prc_ccs_multi_xwalk.R](../data-raw/clean_prc_ccs_multi_xwalk.R)
- rows: 9811, cols: 8
    1. prc_code
    1. ccs_multi_cat_num_1
    1. ccs_multi_cat_name_1
    1. ccs_multi_cat_num_2
    1. ccs_multi_cat_name_2
    1. ccs_multi_cat_num_3
    1. ccs_multi_cat_name_3
    1. flag

Original source: /data/zolab/methods_new/ccs/prc/multi_level/ccs_multi_prc_2015.csv

### prc_ccs_single_xwalk_clean
source code: [data-raw/clean_prc_ccs_single_xwalk.R](../data-raw/clean_prc_ccs_single_xwalk.R)
- rows: 112045, cols: 5
    1. code
    1. ccs_cat
    1. ccs_category_description
    1. icd_9_cm_code_description
    1. flag

Original source: /data/zolab/methods_new/ccs/prc/multi_level/int_xwalk.csv
-  https://gitlab.com/labsysmed/zolab-projects/standard_xwalked/blob/master/pre_xwalk_helpers/hcpcs_multi_xwalk.R
    - converted .feather to .csv

```
rpdr_prc <- readRDS("/data/zolab/master_data/data_repo/partners/up_to_date/data/rpdr_prc/rpdr_prc_raw_mod_bwh_ed_100k.Rds")
prc_ccs_multi_hcpcs <- rpdr_prc[prc_code_type=="CPT" & !is.na(ccs_multi_cat_num_1), c("prc_code", "ccs_multi_cat_num_1", "ccs_multi_cat_name_1", "ccs_multi_cat_num_2", "ccs_multi_cat_name_2", "ccs_multi_cat_num_3", "ccs_multi_cat_name_3")] %>% unique
```

Why did we do this? Jasmeet suspects: (but could be wrong)
- CPT/HCPCS codes do not have a corresponding ccs multi xwalk available for download through the website, so Clara mapped ccs multi categories onto CPT/HCPCS codes.
- we were not able to run her mapping code directly, so we generated a new xwalk based on the codes she had already mapped (which are available in the BWH RPDR data)
