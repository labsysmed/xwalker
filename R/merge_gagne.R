#' merge_gagne
#' @description merge gagne xwalk with dia_code
#'
#' @param DT data.table of dem
#'
#' @return updated DT
#' @export

merge_gagne <- function(DT) {
    DT_gagne <- merge(DT, dia_gagne_xwalk_clean, by.x="dia_code", by.y="code", all.x=TRUE)

    return(DT_gagne)
}
