#' clean_dia_ccs_multi_xwalk
#' @description use raw xwalks downloaded from ccs site to create a cleaned up ccs single xwalk
#'#'
#' @return cleaned up ccs multi xwalk
#' @export

library(stringr)
library(data.table)
source('../R/shorten_label.R')

clean_dia_ccs_multi_xwalk <- function() {
    ### load ccs
    # original source: /data/zolab/methods_new/ccs/dia/multi_level/ccs_multi_dia_2015.csv
    dia_ccs_multi <- suppressWarnings(fread("ccs_multi_dia_2015.csv", quote="\'"))

    ### clean and reformat ccs multi
    dia_ccs_multi %>% colnames %>% sapply(tolower) %>% sapply(function(x) gsub(" |-", "_", x)) %>% setnames(dia_ccs_multi, colnames(dia_ccs_multi), .)  # clean up colnames

    # define description columns
    description_columns <- c("ccs_lvl_1_label", "ccs_lvl_2_label", "ccs_lvl_3_label", "ccs_lvl_4_label")
    dia_ccs_multi[,(description_columns):=lapply(.SD, function(x) shorten_label(x, max_length=50)), .SDcols=description_columns]  # clean up descriptions
    dia_ccs_multi[,icd_9_cm_code:=str_trim(icd_9_cm_code)]

    dia_ccs_multi_xwalk_clean <- dia_ccs_multi
    devtools::use_data(dia_ccs_multi_xwalk_clean, overwrite = TRUE)

    return(dia_ccs_multi_xwalk_clean)
}
